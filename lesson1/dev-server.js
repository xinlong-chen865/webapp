// webpack-dev-middleware
const express = require('express');
let app = express();
const webpack = require('webpack');
const webpackOptions = require('./webpack.config');
const WebpackDevMiddleware = require('webpack-dev-middleware');

webpackOptions.mode = 'development';
const compiler = webpack(webpackOptions);

app.use(WebpackDevMiddleware(compiler, {}));
app.listen(9001);