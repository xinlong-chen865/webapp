const { resolve } = require('path');
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin');
const FilemanagerWebpackPlugin = require('filemanager-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin  = require('optimize-css-assets-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

// webpack --config filename
module.exports = (env) => {
    return {
        mode: 'development',
        devtool: false, // hidden-source-map eval-cheap-module-source-map
        entry: {
            main: './src/index.js',
        },
        optimization: {
            minimize: env && env.production,
            minimizer: [
                new TerserWebpackPlugin(),
                new OptimizeCssAssetsWebpackPlugin(),
            ],
        },
        output: {
            path: resolve(__dirname, 'dist'),
            filename: '[name].[hash:10].js',
            // publicPath: './',
        },
        // sideEffects: [
        //     "**/*.css",
        //     "**/*.scss",
        // ],
        devServer: {
            static: {
                directory: resolve(__dirname, 'static'),
            },
            compress: true,
            port: 8080,
            open: true,
            proxy: {
                '/api': 'http://localhost:3000'
            }
        },
        // 监听
        // watch: true,
        // watchOptions: {
        //     ignored: /node_modules/,
        //     aggregateTimeout: 300,
        //     poll: 1000
        // },
        // 外部引入
        // externals: {
        //     lodash: '_',
        // },
        module: {
            rules: [
                // {
                //     test: require.resolve('lodash'),
                //     loader: 'expose-loader',
                //     options: {
                //         exposes: {
                //             globalName: '_',
                //             override: true,
                //         }
                //     }
                // },
                // {  
                //     test: /\.jsx?$/,
                //     loader: 'eslint-loader',
                //     enforce: 'pre',
                //     options: { fix: true }, // 启动自动修复
                //     // exclude: /node_modules/,
                //     include: resolve(__dirname, 'src')
                // },
                { test: /\.jsx?$/, 
                  exclude: /node_modules/, // 解决core-js的问题
                  use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ["@babel/preset-env", {
                                    useBuiltIns: 'usage', // 按需加载
                                    corejs: { version: '3' },
                                    targets: ">0.25%" // 也可以使用，polyfill-io外网链接
                                }], // js
                                "@babel/preset-react" // jsx
                            ],
                            plugins: [
                                [
                                    '@babel/plugin-transform-runtime',
                                    {
                                        corejs: false,
                                        helpers: true,
                                        regenerator: true,
                                    }
                                ], // promise || 避免全局污染
                                ["@babel/plugin-proposal-decorators", { legacy: true }],
                                ["@babel/plugin-proposal-class-properties", { loose: true }],
                            ]
                        }
                    }
                ] },
                { test: /\.txt$/, use: 'raw-loader' },
                // 用MiniCssExtractPlugin.loader替换style-loader
                { 
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        // 'style-loader',
                        'css-loader',
                        'postcss-loader',
                        {
                            loader: 'px2rem-loader',
                            options: {
                                remUnit: 75,
                            }
                        }
                    ] 
                },
                { test: /\.less$/, use: ['style-loader', 'css-loader', 'postcss-loader', 'less-loader'] },
                { test: /\.scss$/, use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'] },
                {   
                    test: /\.(jpg|png|gif|jpeg|bmp)$/,
                    use: [{
                        loader: 'url-loader',
                        options: {
                            name: '[hash:10].[ext]',
                            esModule: false,
                            limit: 8 * 1024, // 优化
                            outputPath: 'images',
                            // publicPath: './images',
                            // 如果这里设置了publicPath，MiniCssExtractPlugin的publicPath就会失效
                        },
                    }],
                    type: 'javascript/auto',
                },
                { test: /\.html$/, use: ['html-withimg-loader'] }, // 解析img中的相对路径图片 html-loader换成html-withimg-loader
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html',
                minify: {
                    collapseWhitespace: true, // 空格
                    removeComments: true, // 注释
                }
                // chunks: []
            }),
            // new webpack.ProvidePlugin({ // 插件引入
            //     _: 'lodash'
            // })
            new HtmlWebpackExternalsPlugin({
                externals: [
                    {
                        module: 'lodash',
                        entry: env.production ? 'https://cdn.bootcdn.net/ajax/libs/lodash.js/4.17.21/lodash.js' : 'http://localhost:8080/lodash.js',
                        global: '_',
                    }
                ]
            }),
            new webpack.DefinePlugin({ // 声明模块的全局变量
                DEVELOPMENT: JSON.stringify(!env.production),
                VERSION: "1",
                COPYRIGHT: {
                    name: JSON.stringify("chenxinlong"),
                }
            }),
            new webpack.SourceMapDevToolPlugin({
                filename: '[file].map',
                append: "\n//# sourceMappingURL=http://localhost:8081/[url]"
            }),
            new FilemanagerWebpackPlugin({
                events: {
                    onEnd: {
                        copy: [
                            {
                                source: './dist/**/*.map',
                                destination: './sourcemap/'
                            }
                        ],
                        delete: ['./dist/*.map']
                    }
                }
            }),
            new CopyWebpackPlugin({
                patterns: [
                    {
                        from: resolve(__dirname, 'src/documents'),
                        to: resolve(__dirname, 'dist/documents'),
                    }
                ]
            }),
            new CleanWebpackPlugin({
                cleanOnceBeforeBuildPatterns: ["**/*"]
            }),
            new MiniCssExtractPlugin({
                filename: 'css/[name].css'
            })
        ]
    }
};